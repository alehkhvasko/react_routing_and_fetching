import { BrowserRouter, Routes, Route } from "react-router-dom";
import { PostList } from "./components/PostList";
import { ToDoList } from "./components/ToDoList";
import { UserList } from "./components/UserList";
import { Navigation } from "./components/Navigation";
import { AppRoutes } from "./AppRoutes";
function App() {
  const { Post, ToDo, User } = AppRoutes;
  return (
    <BrowserRouter>
      <Navigation />
      <Routes>
        <Route path={Post} element={<PostList />} />
        <Route path={ToDo} element={<ToDoList />} />
        <Route path={User} element={<UserList />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
