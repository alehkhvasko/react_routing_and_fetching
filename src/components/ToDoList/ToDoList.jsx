import { useFetch } from "../fetchAPI/useFetch";

export const ToDoList = () => {
  const { error, isLoading, data } = useFetch(
    "https://jsonplaceholder.typicode.com/posts"
  );
  if (error) {
    return <div>Error: {error}</div>;
  }
  return (
    <div>
      {isLoading ? (
        <div>Loading...</div>
      ) : (
        <div>
          <h1>To Do List </h1>
          {data?.map((todo, index) => (
            <li style={{ listStyleType: "none" }} key={todo.id}>
              {index + 1}) Name: {todo.title}, status:
              {<p>{todo.completed ? "completed" : "incomplete"}</p>}
              <br />
            </li>
          ))}
        </div>
      )}
    </div>
  );
};
