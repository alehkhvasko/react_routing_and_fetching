//import { useEffect, useState } from "react";
import { useFetch } from "../fetchAPI/useFetch";

export const PostList = () => {
  const { error, isLoading, data } = useFetch(
    "https://jsonplaceholder.typicode.com/posts"
  );
  if (error) {
    return <div>Error: {error}</div>;
  }
  return (
    <div>
      {isLoading ? (
        <div>Loading...</div>
      ) : (
        <div>
          <h1>Post List</h1>
          <ul style={{ textAlign: "left" }}>
            {data?.map((posts, index) => (
              <li style={{ listStyleType: "none" }} key={posts.id}>
                {index + 1}) {posts.title}
                <br />
              </li>
            ))}
          </ul>
        </div>
      )}
    </div>
  );
};
